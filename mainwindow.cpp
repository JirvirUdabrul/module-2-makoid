#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QRegularExpression>
#include <QTextEdit>


double value;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_toKilo_clicked()
{
    QString s = ui->str->text();
    if(!s.contains(QRegularExpression("[A-Za-z]")))
    {
       value = ui->str->text().toDouble();
       value *=0.45359237;
       s = QString::number(value, 'g', 10);
       ui->result->setText(s);
    }
    else
    {
        ui->result->setText("ERROR!");
    }
}


void MainWindow::on_toGrams_clicked()
{
    QString s = ui->str->text();
    if(!s.contains(QRegularExpression("[A-Za-z]")))
    {
       value = ui->str->text().toDouble();
       value *= 453,59237;
       s = QString::number(value, 'g', 10);
       ui->result->setText(s);
    }
    else
    {
        ui->result->setText("ERROR!");
    }
}




void MainWindow::on_toMiligrams_clicked()
{
    QString s = ui->str->text();
    if(!s.contains(QRegularExpression("[A-Za-z]")))
    {
       value = ui->str->text().toDouble();
       value *= 453592.37;
       s = QString::number(value, 'g', 10);
       ui->result->setText(s);
    }
    else
    {
        ui->result->setText("ERROR!");
    }
}


void MainWindow::on_Clear_clicked()
{
    ui->result->setText("0");
    ui->str->clear();
}

