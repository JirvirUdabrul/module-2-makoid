#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private:
    Ui::MainWindow *ui;
private slots:

    void on_toKilo_clicked();
    void on_toGrams_clicked();
    void on_toMiligrams_clicked();
    void on_Clear_clicked();
};
#endif // MAINWINDOW_H
